using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    InputInvoker m_inputInvoker;


    // Start is called before the first frame update
    void Start()
    {
        m_inputInvoker = new InputInvoker(new Dictionary<string, ICommand>()
        {
            { "Jump", new JumpCommand(GetComponent<Rigidbody2D>()) },
        });    
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_inputInvoker.Invoke("Jump");
        }
    }
}
