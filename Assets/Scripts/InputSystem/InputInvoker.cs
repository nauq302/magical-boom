using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputInvoker
{
    private Dictionary<string, ICommand> m_commands;

    public InputInvoker(Dictionary<string, ICommand> commands)
    {
        this.m_commands = commands;
    }

    public void Invoke(string commandName)
    {
        if (m_commands.ContainsKey(commandName))
        {
            m_commands[commandName].Execute();
        }
    }
    
}
