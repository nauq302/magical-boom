using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCommand : ICommand
{
    private Rigidbody2D rigidbody2D;
    private float jumpForce;

    public JumpCommand(Rigidbody2D rigidbody2D, float jumpForce = 10.0f)
    {
        this.rigidbody2D = rigidbody2D;
        this.jumpForce = jumpForce;
    }

    public void Execute()
    {
        Debug.Log("JumpCommand.Execute()");
        rigidbody2D.AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
    }
}

