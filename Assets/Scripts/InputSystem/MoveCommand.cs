using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MoveCommand : ICommand
{
    public enum MoveDirection {
        UP,
        DOWN,
        LEFT,
        RIGHT,
    }

    private readonly Rigidbody2D rigidbody2D;
    private readonly MoveDirection direction;
    
        
    public MoveCommand(Rigidbody2D rigidbody2D, MoveDirection direction) 
    {
        this.rigidbody2D = rigidbody2D;
        this.direction = direction;
    }


    public void Execute()
    {
        switch (direction)
        {
            case MoveDirection.UP:
                rigidbody2D.AddForce(Vector3.up * 10.0f, ForceMode2D.Impulse);
                break;
            case MoveDirection.DOWN:
                rigidbody2D.AddForce(Vector3.down * 10.0f, ForceMode2D.Impulse);
                break;
            case MoveDirection.LEFT:
                rigidbody2D.AddForce(Vector3.left * 10.0f, ForceMode2D.Impulse);
                break;
            case MoveDirection.RIGHT:
                rigidbody2D.AddForce(Vector3.right * 10.0f, ForceMode2D.Impulse);
                break;
        }
    }
}
